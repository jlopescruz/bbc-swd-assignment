# README #

### NOTES ###

This app is currently doing:

- A Grunt build has been set up.
- Loading json data from BBC server and showing that data on different pages.
- The pages carousel is dynamic and functional.
- The app layout is optimized for iPad resolution (768 x 1024).
- The Unit and E2E tests are already set up and ready to be implemented.

Missing features / todo list:

- Layout improvements - matching the design
- Article detail
- Video and audio playback
- Swipe events implementation




### SET UP ###

1 - Install node modules
```shell
npm install
```

2 - Run front-end tasks
```shell
grunt
```

3 - Run JS Unit tests
```shell
grunt unit
```

4 - Run E2E tests
```shell
grunt e2e
```




### FRONT-END TECHONOLOGIES ###

- HTML5
- CSS3
- CSS Bootstrap
- Grunt
- SASS
- AngularJS
- Karma - Unit and E2E testing
- Jasmine




### ANGULARJS APP ARCHITECTURE ###

This is an AngularJS app with the following structure:

![Alt text](https://dl.dropboxusercontent.com/u/1843460/diagram.jpg)





### WEB APP ###
After a successfull grunt build, the app will be live on:

```shell
http://localhost/bbc-swd-assignment/dist/
```